var local = localStorage;
var json = [{
    nom: "Produit Vert",
    prix: "10 €",
    description: "Le Spray Magique 'Produit Vert'. Transformez instantanément la saleté en éclat avec notre formule magique ! Dites adieu aux taches tenaces et bonjour à une maison étincelante en un clin d'œil.",
    link : "img/Produit_Vert.png" 
}, {
    nom: "Produit Jaune",
    prix: "12 €",
    description: "Dépoussiérant 'Produit Jaune'. La poussière n'a aucune chance contre notre Dust-Buster. Vous serez tellement obsédé par la propreté que vous chercherez constamment de la poussière à éliminer !",
    link : "img/Produit_Jaune.png" 
}, {
    nom: "Produit Bleu",
    prix: "20 €",
    description: "Liquide Vaisselle 'Produit Bleu'. Faites briller chaque assiette et chaque verre avec le pouvoir de SparkleWave. Transformez la vaisselle ennuyeuse en une œuvre d'art éclatante !",
    link : "img/Produit_Bleu.png" 
}, {
    nom: "Produit Blanc",
    prix: "900 €",
    description: "Le Désinfectant 'Produit Blanc'. Dites au revoir aux germes indésirables avec GermGuard. Votre maison sera plus propre que jamais, et vous vous sentirez invincible.",
    link : "img/Produit_Blanc.png" 
}, {
    nom: "Produit Rose",
    prix: "9 €",
    description: "Le Spray Magique 'Produit Rose'. Transformez instantanément la saleté en éclat avec notre formule magique ! Dites adieu aux taches tenaces et bonjour à une maison étincelante en un clin d'œil.",
    link : "img/Produit_Rose.png" 
}, {
    nom: "Produit Cyan",
    prix: "16 €",
    description: "Dépoussiérant 'Produit Cyan'. La poussière n'a aucune chance contre notre Dust-Buster. Vous serez tellement obsédé par la propreté que vous chercherez constamment de la poussière à éliminer !",
    link : "img/Produit_Cyan.png" 
},
];

popUp(!local.getItem("modal"));

var deconnexion = document.getElementById("deconnexion");
var dropdowns = document.querySelectorAll('.dropdown-menu');
var toggle = document.getElementById("navbarDropdownMenuLink");

let showMenu = false;

toggle.addEventListener('click', function (event) {
    event.preventDefault();
    if(showMenu === true){
        dropdowns.forEach(function(dropdown) {
        dropdown.classList.remove('show');
        dropdown.classList.add('hide');
        showMenu = false;
    });
    }else{
        dropdowns.forEach(function(dropdown) {
        dropdown.classList.remove('hide');
        dropdown.classList.add('show');
        showMenu = true;
    });
    }
});

function setProduct(id) {
    let div = document.getElementById("detail");
    div.innerHTML = '<div class="card mx-auto" style="max-width: 500px;">' +
        '<div class="card-body">' +
        '<h5 class="card-title">' + json[id].nom + '</h5>' +
        '<center><img src="' + json[id].link + '" height="auto" alt="Image du Produit"></center><br>' +
        '<span>Description :</span>' +
        '<p>&nbsp; ' + json[id].description + '</p>' +
        '<span>Prix :</span>' +
        '<p>&nbsp; ' + json[id].prix + '</p>' +
        '</div>' +
        '</div>'
}

function popUp(enable) {
    var modal = document.getElementById('modal');
    if (enable) {
        if (!modal) {
            modal = document.createElement('div');
            modal.className = 'modal fade';
            modal.id = 'modal';
            modal.setAttribute('tabindex', '-1');
            modal.setAttribute('role', 'dialog');
            modal.innerHTML = '<div class="modal-dialog" role="document">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<h5 class="modal-title">Confidentialité</h5>' +
                '</div>' +
                '<div class="modal-body">' +
                'En cliquant sur "Suivant", vous acceptez toutes les conditions d\'utilisation du site' +
                '</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-primary" onclick="popUp(false)" data-dismiss="modal">Suivant</button>' +
                '</div>' +
                '</div>' +
                '</div>';

            document.body.appendChild(modal);
            local.setItem("modal", false);

            modal.classList.add('show');
            modal.style.display = 'block';
            modal.style.backgroundColor = 'rgba(0,0,0,0.5)';
            modal.style.overflow = 'auto';
        }
    } else {
        if (modal) {
            modal.classList.remove('show');
            modal.style.display = 'none';
            modal.style.backgroundColor = '';
            modal.style.overflow = '';

        }
    }
}

var latitude = 48.858844;
var longitude = 2.294351;

var map = L.map('map').setView([latitude, longitude], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([latitude, longitude]).addTo(map)
    .bindPopup('Siège Social Bros-tastique')
    .openPopup();