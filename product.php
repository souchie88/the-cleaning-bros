<!DOCTYPE html>
<html lang="en">

    <?php
        require "head.php"; // appel du fichier head.php
    ?>

    <head>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
            integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Produits</title>
    </head>

    <body>

        <div class="search-product">
            <center><label for="name">Rechercher un produit</label>
            <input type="text" id="liste" name="liste" class="form-control" style="width: 30%;" placeholder="Exemple : Rose" /></center>
        </div>

        <?php
            $sql = "SELECT * FROM produit";
            $result = $link->query($sql);

            
            if ($result->num_rows > 0) {
                echo "<ul>";
                while ($row = $result->fetch_assoc()) {
                    ?>
                    <li>
                        <a href='details.php?id=<?=$row["id"]?>'>
                            <?php
                                echo "<img src='img/" . $row["image"] . "' alt='" . $row["name"] . "'><br>";
                                echo "<p>" . $row["name"] . "<br>";
                                echo "" . $row["price"] . "€</p>";
                            ?>
                        </a>
                    </li>
                    <?php
                }
                echo "</ul>";
            } else {
                echo "Aucun produit trouvé.";
            }
        ?>

    </body>

    <script src="script.js"></script>
    <script src="script2.js"></script>
    <script src="script3.js"></script>
    
    <?php
        require './footer.php';
    ?>
</html>