<!DOCTYPE html>
<html>

<?php
require "head.php";
require "dbutilisateur.php";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $pseudo = $_POST['pseudo'];
    $password = $_POST['password'];

    if(loginUser($pseudo, $password)) {
        $_SESSION["user"] = $pseudo;
        header("Location: index.php");
    }
}
?>

<head>
    <title>The Cleaning Bros</title>

</head>

<body>
<header class="bg-primary text-white text-center py-5">
    <h1 id="index-welcome-title">Bienvenue chez The Cleaning Bros</h1>
    <span id="index-welcome-sub">Votre source de produits ménagers de qualité</span>
</header>

<section class="container my-5">
    <h2 id="index-who-title" class="text-center">Qui sommes-nous ?</h2>
    <span id="index-who-1">Nous sommes "The Cleaning Bros", votre partenaire pour tout ce qui concerne le nettoyage et l'entretien de votre maison. Notre mission est de vous fournir les meilleurs produits ménagers de qualité supérieure pour rendre votre environnement propre, frais et accueillant.</span>
    <span id="index-who-2">Que vous ayez besoin de nettoyants tout usage, de détergents à lessive, de produits pour vitres, ou de tout autre article de nettoyage, nous avons ce qu'il vous faut. Notre équipe s'engage à vous offrir des produits sûrs, efficaces et respectueux de l'environnement.</span>
    <span id="index-who-3">Explorez notre catalogue en ligne, commandez vos produits préférés en quelques clics et profitez de la livraison à domicile pour une expérience de nettoyage sans tracas.</span>
</section>


    <div class="container">
        <h2 id="index-command-title" class="text-center">Commandez en Ligne</h2>
        <center><span id="index-command-sub" class="text-center">Faites vos achats en ligne chez The Cleaning Bros et profitez de la commodité de la livraison à domicile.</span></center>
        <div class="text-center"><br>
            <a href="product.php" class="btn btn-primary" id="index-command-start">Commencer vos achats</a>
        </div>
    </div>


</body>
<script src="script.js"></script>
<script src="script2.js"></script>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
    integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <?php
        require './footer.php';
    ?>

</html>