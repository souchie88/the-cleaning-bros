<!DOCTYPE html>
<html>

<?php
require "head.php";
?>

<header>
    <meta charset="UTF-8">
</header>

<head>
    <title>Connexion</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>
    <center><h1>Connexion</h1></center>
    <div class="container mt-5">
    <form action="index.php" method="post">

        <div class="form-group">
            <label for="login">Pseudo :</label>
            <input type="text" class="form-control" id="login" name="pseudo" placeholder="Entrez votre pseudo" required>
        </div>

        <br>
        <div class="form-group">
            <label for="password">Mot de passe :</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Entrez votre mot de passe" required>  
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Se connecter</button>
    
    </form>
</div>
    </body>
    <script src="script.js"></script>
    <script src="script2.js"></script>
</html>