<!DOCTYPE html>
<html>

<?php
require "head.php";
require "dbutilisateur.php";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $pseudo = $_POST['pseudo'];
    $password = $_POST['password'];
    $email = $_POST['email'];
    $admin = 0;

    if(registerUser($pseudo, $password, $email, $admin)) {
        $_SESSION['registration_success'] = true;
    } else {
        $_SESSION['registration_success'] = false;
    }
}
?>

<header>
    <meta charset="UTF-8">
</header>

    <head>
        <title>Inscription</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    </head>
    <body>
        <center><h1>Inscription</h1></center>

        <div class="container mt-5">
    <form action="inscription.php" method="post">

        <div class="form-group">
            <label for="login">Pseudo :</label>
            <input type="text" class="form-control" id="login" name="pseudo" placeholder="Entrez votre pseudo" required>
        </div>

        <br>
        <div class="form-group">
            <label for="password">Mot de passe :</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Entrez votre mot de passe" required>
        </div>
        <br>

        <div class="form-group">
            <label for="email">Email :</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="exemple@domaine.com" required>
        </div>
        <br>
        <button type="submit" class="btn btn-primary">S'inscrire</button>
    
    </form>
    <?php
    if (isset($_SESSION['registration_success'])) {
        if ($_SESSION['registration_success'] === true) {
            echo "<br><center><span style='color:green'>Votre compte à été créé</span></center>";
        }
        unset($_SESSION['registration_success']);
    }

    ?>
</div>

    </body>
    <script src="script.js"></script>
    <script src="script2.js"></script>

    <?php
        require './footer.php';
    ?>
    
</html>