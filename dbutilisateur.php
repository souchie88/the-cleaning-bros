<?php

function registerUser($pseudo, $password, $email, $admin) {
    global $link;

    $hashedPassword = password_hash($password, PASSWORD_BCRYPT);

    $stmt = mysqli_prepare($link, "INSERT INTO utilisateur (pseudo, password, email, admin) VALUES (?, ?, ?, ?)");

    if (!$stmt) {
        die("Erreur lors de la préparation de la requête : " . mysqli_error($link));
    }

    mysqli_stmt_bind_param($stmt, "sssi", $pseudo, $hashedPassword, $email, $admin);

    if (mysqli_stmt_execute($stmt)) {
        return true;
    } else {
        die("Erreur lors de l'insertion : " . mysqli_stmt_error($stmt));
    }
}

function loginUser($pseudo, $password) {
    global $link;

    $stmt = $link->prepare("SELECT * FROM utilisateur WHERE pseudo = ?");
    $stmt->bind_param("s", $pseudo);
    $stmt->execute();

    $result = $stmt->get_result();
    if($user = $result->fetch_assoc()) {
        if(password_verify($password, $user["password"])) {
            $_SESSION["pseudo"] = $user["pseudo"];
            $_SESSION["admin"] = $user["admin"];
            $stmt->close();
            return true;
        }
    }

    $stmt->close();
    return false;
}

?>
