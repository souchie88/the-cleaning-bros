const input = document.getElementById('liste');
const listeLi = document.querySelectorAll('ul li');

input.addEventListener('input', function() {
    const recherche = input.value.toLowerCase();

    listeLi.forEach(li => {
        const texteLi = li.textContent.toLowerCase();

        if (texteLi.includes(recherche)) {
            li.style.display = 'inline-block';
        } else {
            li.style.display = 'none';
        }
    });
});