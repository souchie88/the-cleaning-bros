<script>
    var languageSelect = document.getElementById("language-select");

    if(localStorage.getItem('language')){
        tradOnChange(localStorage.getItem('language'));
        languageSelect.value = localStorage.getItem('language');
    }else{
        localStorage.setItem('language', 'fr');
        tradOnChange(localStorage.getItem('language'));
        languageSelect.value = localStorage.getItem('language');
    }
    

    languageSelect.addEventListener("change", function() {
        localStorage.setItem('language', languageSelect.value);
        let selectedLanguage = languageSelect.value;
        tradOnChange(selectedLanguage);
        });

        function tradOnChange(langue){
            const content = {
                'fr': {
                    'accueil': 'Accueil',
                    'produits': 'Produits',
                    'contact': 'Contact',
                    'inscription': 'Inscription',
                    'connexion': 'Connexion',
                    'welcome-title': 'Bienvenue chez The Cleaning Bros',
                    'welcome-sub': 'Votre source de produits ménagers de qualité',
                    'index-who-title': 'Qui sommes-nous ?',
                    'index-who-1': "Nous sommes 'The Cleaning Bros', votre partenaire pour tout ce qui concerne le nettoyage et l'entretien de votre maison. Notre mission est de vous fournir les meilleurs produits ménagers de qualité supérieure pour rendre votre environnement propre, frais et accueillant.",
                    'index-who-2': "Que vous ayez besoin de nettoyants tout usage, de détergents à lessive, de produits pour vitres, ou de tout autre article de nettoyage, nous avons ce qu'il vous faut. Notre équipe s'engage à vous offrir des produits sûrs, efficaces et respectueux de l'environnement.",
                    'index-who-3': "Explorez notre catalogue en ligne, commandez vos produits préférés en quelques clics et profitez de la livraison à domicile pour une expérience de nettoyage sans tracas.",
                    'index-command-title': "Commandez en Ligne",
                    'index-command-sub': "Faites vos achats en ligne chez The Cleaning Bros et profitez de la commodité de la livraison à domicile.",
                    'index-command-start': "Commencer vos achats",
                },
                'en': {
                    'accueil': 'Home',
                    'produits': 'Products',
                    'contact': 'Contact',
                    'inscription': 'Registration',
                    'connexion': 'Login',
                    'welcome-title': 'Welcome to The Cleaning Bros',
                    'welcome-sub': 'Your source for quality household products',
                    'index-who-title': 'Who are we ?',
                    'index-who-1': "We are 'The Cleaning Bros', your partner for everything related to cleaning and maintaining your home. Our mission is to provide you with the best top quality household products to make your environment clean, fresh and welcoming.", 
                    'index-who-2': "Whether you need all-purpose cleaners, laundry detergents, window products, or any other cleaning item, we have what you need. Our team is committed to offering you safe, effective and environmentally friendly products.",
                    'index-who-3': "Browse our online catalog, order your favorite products in a few clicks and enjoy home delivery for a hassle-free cleaning experience.",
                    'index-command-title': "Command on Lign",
                    'index-command-sub': "Shop online at The Cleaning Bros and enjoy the convenience of home delivery.",
                    'index-command-start': "Start your shopping",
                }
            };

            document.querySelector("a[href='index.php']").textContent = content[langue]['accueil'];
            document.querySelector("a[href='product.php']").textContent = content[langue]['produits'];
            document.querySelector("a[href='contact.php']").textContent = content[langue]['contact'];
            document.querySelector("a[href='inscription.php']").textContent = content[langue]['inscription'];
            document.querySelector("a[href='connexion.php']").textContent = content[langue]['connexion'];
            document.querySelector("#index-welcome-title").textContent = content[langue]['welcome-title'];
            document.querySelector("#index-welcome-sub").textContent = content[langue]['welcome-sub'];
            document.querySelector("#index-who-title").textContent = content[langue]['index-who-title'];
            document.querySelector("#index-who-1").textContent = content[langue]['index-who-1'];
            document.querySelector("#index-who-2").textContent = content[langue]['index-who-2'];
            document.querySelector("#index-who-3").textContent = content[langue]['index-who-3'];
            document.querySelector("#index-command-title").textContent = content[langue]['index-command-title'];
            document.querySelector("#index-command-sub").textContent = content[langue]['index-command-sub'];
            document.querySelector("#index-command-start").textContent = content[langue]['index-command-start'];
        }

</script>