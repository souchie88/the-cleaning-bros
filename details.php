<!DOCTYPE html>
<html lang="en">

    <?php
        require "head.php"; // appel du fichier head.php

        if (isset($_GET["id"])) {
            $product_id = $_GET["id"];

        } else {
            echo "ID de produit non spécifié.";
        }
    ?>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Détails</title>
    </head>

    <body>
        <div id="detail" class="container details">
            <?php
                $sql = "SELECT * FROM produit WHERE id = $product_id";
                $result = $link->query($sql);
                $row = $result->fetch_assoc();

                echo "<img src='img/" . $row["image"] . "' alt='" . $row["name"] . "'><br>";
                echo "<p>" . $row["name"] . "<br>";
                echo $row["price"] . "€</p>";
                echo "<p>" . $row["description"] . "</p>";
            ?>
        </div>
    </body>

    <script src="script.js"></script>
    <script src="script2.js"></script>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <?php
        require './footer.php';
    ?>

</html>