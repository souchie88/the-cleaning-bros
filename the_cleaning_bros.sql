-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 04 oct. 2023 à 14:52
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `the_cleaning_bros`
--

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id`, `name`, `description`, `price`, `image`) VALUES
(1, 'Produit Rose', ' Le Spray Magique \'Produit Rose\'. Transformez instantanément la saleté en éclat avec notre formule magique ! Dites adieu aux taches tenaces et bonjour à une maison étincelante en un clin d\'œil.', 9, 'Produit_Rose.png'),
(2, 'Produit Blanc', '  Le Désinfectant \'Produit Blanc\'. Dites au revoir aux germes indésirables avec GermGuard. Votre maison sera plus propre que jamais, et vous vous sentirez invincible.', 6, 'Produit_Blanc.png'),
(3, 'Produit Bleu', 'Liquide Vaisselle \'Produit Bleu\'. Faites briller chaque assiette et chaque verre avec le pouvoir de SparkleWave. Transformez la vaisselle ennuyeuse en une œuvre d\'art éclatante !', 5, 'Produit_Bleu.png'),
(4, 'Produit Cyan', 'Dépoussiérant \'Produit Cyan\'. La poussière n\'a aucune chance contre notre Dust-Buster. Vous serez tellement obsédé par la propreté que vous chercherez constamment de la poussière à éliminer !', 12, 'Produit_Cyan.png'),
(5, 'Produit Jaune', 'Dépoussiérant \'Produit Jaune\'. La poussière n\'a aucune chance contre notre Dust-Buster. Vous serez tellement obsédé par la propreté que vous chercherez constamment de la poussière à éliminer !', 6, 'Produit_Jaune.png'),
(6, 'Produit Vert', 'Le Spray Magique \'Produit Vert\'. Transformez instantanément la saleté en éclat avec notre formule magique ! Dites adieu aux taches tenaces et bonjour à une maison étincelante en un clin d\'œil.', 7, 'Produit_Vert');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `pseudo`, `password`, `email`, `admin`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
