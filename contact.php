<!DOCTYPE html>
<html>

<?php
require "head.php"; // appel du fichier head.php
?>

<head>
    <title>Contact</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
</head>

<body class="contact">
    <h1>Contact</h1>

    <h2>📞 Service Client Bros-tastique 📞</h2>

    <p>Besoin d'aide ou de conseils sur nos produits ? <br>Appelez notre équipe Bros-tastique au 01 23 45 67 89. <br>Nous sommes là pour vous !</p>

    <div id="map"></div>
    
    <script src="script.js"></script>
    <script src="script2.js"></script>
</body>

    <?php
        require './footer.php';
    ?>

</html>