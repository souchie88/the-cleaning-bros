

<?php
define('PATH', str_replace($_SERVER['DOCUMENT_ROOT'], $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'], "http://".__DIR__."/"));
define ('DB_HOST', "localhost");
define ('DB_USER', "root");
define ('DB_PWD', "");
define ('DB_NAME', "the_cleaning_bros");

$link = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_NAME);

if (!$link) {
    die("Erreur de connexion à la base de données : " . mysqli_connect_error());
}
?>
