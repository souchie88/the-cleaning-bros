<?php
session_start();
require "config.php";
?>

<header>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <div class="navbar">
        <ul>
            <img src="img/logo.png" alt="" class="logo">
            <a class="nav" href="index.php">Accueil</a>
            <a class="nav" href="product.php">Produits</a>
            <a class="nav" href="contact.php">Contact</a>
            <?php             
            if (!isset($_SESSION["user"])) {
                echo '<a class="nav" href="inscription.php">Inscription</a></li>';
                echo '<a class="nav" href="connexion.php">Connexion</a></li>';
            } else {
                if($_SESSION["admin"] === 1){
                    echo '<a class="nav" href="admin.php">Administration</a>';
                } 
                echo '<div style="float:left" class="dropdown">';
                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">' . $_SESSION["user"] .  '</a>';
                echo '<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">';
                echo '<span class="dropdown-item" href="#">Profil</span>';
                echo '<span class="dropdown-item" href="#">Paramètres</span>';
                echo '<a class="dropdown-item" href="deconnexion.php">Déconnexion</a>';
                echo '</div>';
                echo '</div>';        
            }
            ?>
        </ul>
    </div>
</body>
    <div class="language-selector">
        <select id="language-select" name="language" class="language-selector">
            <option value="fr" <?php if(isset($_POST['language']) && $_POST['language'] === 'fr') echo 'selected'; ?>>FR</option>
            <option value="en" <?php if(isset($_POST['language']) && $_POST['language'] === 'en') echo 'selected'; ?>>EN</option>
        </select>
    </div>

    <?php
        require "change-language.php";
    ?>

    <a id="darkModeButton" class="darkmode"><img src="img/daymode.png" class="darkmode"></a>
</header>
