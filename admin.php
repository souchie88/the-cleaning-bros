<!DOCTYPE html>

<?php
require "head.php";
?>

<html lang="en">

<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Page</title>


    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        .action-button {
            color: white !important;
            text-decoration: none;
        }

        .td-admin {
            color: red !important;
        }
    </style>


</head>


<body>



</body>

<?php
$sql = "SELECT id, pseudo, password, email, admin FROM utilisateur";
$result = $link->query($sql);


// Fonction pour générer un lien d'ajout d'administrateur
function generateAddAdminLink($id)
{
    return "<button class='btn btn-primary'><a class='action-button' href='admin.php?action=ajouter_admin&id=$id'>Ajouter Admin</a></button>";
}

// Fonction pour générer un lien de suppression d'administrateur
function generateDeleteAdminLink($id)
{
    return "<button class='btn btn-primary'><a class='action-button' href='admin.php?action=supprimer_admin&id=$id'>Supprimer Admin</a></button>";
}


// Fonction pour générer un lien de suppression
function generateDeleteLink($id)
{
    return "<button class='btn btn-primary'><a class='action-button' href='admin.php?action=supprimer_utilisateur&id=$id'>Supprimer</a></button>";
}



// Gestion des actions
if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if ($action == 'ajouter_admin' && isset($_GET['id'])) { // ajout de l'admin
        $id = $_GET['id'];
        $sql = "UPDATE utilisateur SET admin = 1 WHERE id = $id";
        if ($link->query($sql) === TRUE) {
            echo "L'utilisateur a été ajouté en tant qu'administrateur avec succès.<br>";
        } else {
            echo "Erreur lors de l'ajout en tant qu'administrateur : " . $link->error;
        }
    } elseif ($action == 'supprimer_utilisateur' && isset($_GET['id'])) { // supprimer un utilisateur
        $id = $_GET['id'];
        $sql = "DELETE FROM utilisateur WHERE id = $id";
        if ($link->query($sql) === TRUE) {
            echo "L'utilisateur a été supprimé avec succès.<br>";
        } else {
            echo "Erreur lors de la suppression de l'utilisateur : " . $link->error;
        }
    } elseif ($action == 'supprimer_admin' && isset($_GET['id'])) { // supprimer un admin
        $id = $_GET['id'];
        $sql = "UPDATE utilisateur SET admin = 0 WHERE id = $id";
        if ($link->query($sql) === TRUE) {
            echo "L'utilisateur a été retiré du statut d'administrateur avec succès.<br>";
        } else {
            echo "Erreur lors de la suppression du statut d'administrateur : " . $link->error;
        }
    }
}

// Requête SQL pour récupérer les utilisateurs
$sql = "SELECT id, pseudo, password, email, admin FROM utilisateur";
$result = $link->query($sql);





// Vérifier si la requête a renvoyé des résultats
if ($result->num_rows > 0) {
    // Afficher les données dans un tableau
    echo "<table>";
    echo "<tr><th>ID</th><th>Pseudo</th><th>Password</th><th>Email</th><th class='td-admin'>Admin</th><th>Supprimer</th><th>Ajouter Admin</th><th>Supprimer Admin</th></tr>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["id"] . "</td><td>" . $row["pseudo"] . "</td><td>" . $row["password"] . "</td><td>" . $row["email"] . "</td><td class='td-admin'>" . $row["admin"] . "</td><td>" . generateDeleteLink($row["id"]) . "</td><td>" . generateAddAdminLink($row["id"]) . "</td><td>" . generateDeleteAdminLink($row["id"]) . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "Aucun résultat trouvé";
}
?>

    <?php
        require './footer.php';
    ?>

</html>