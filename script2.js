var darkModeButton = document.getElementById('darkModeButton');
const body = document.body;

function enableDarkMode() {
    body.style.backgroundColor = 'grey';
    body.style.color = 'white';
    localStorage.setItem('darkModeButton', 'true');
}

function disableDarkMode() {
    body.style.backgroundColor = 'white';
    body.style.color = 'black';
    localStorage.setItem('darkModeButton', 'false');
}

darkModeButton.addEventListener('click', () => {
    if (body.style.backgroundColor === 'grey') {
        disableDarkMode();
        darkModeButton.innerHTML = '<img src="img/nightmode.png" class="darkmode">';
    } else {
        enableDarkMode();
        darkModeButton.innerHTML = '<img src="img/daymode.png" class="darkmode">';
    }
});

if (localStorage.darkModeButton === 'true') {
    enableDarkMode();
} else {
    disableDarkMode();
}